class String
  def blank?
    self !~ /[^\s]/
  end
end
