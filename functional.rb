states = ["Kansas", "Nebraska", "North Dakota", "South Dakota"]
numbers = 1..10

# Returns a URL-friendly version of a string
#  Example: "North Dakota" -> "north-dakota"
def urlify(string)
  string.downcase.split.join('-')
end


# URLs: Imperative Version
def imperative_urls(states)
  urls = []
  states.each do |state|
    urls << urlify(state)
  end
  urls
end
puts imperative_urls(states).inspect


# URLs: Functional Version
def functional_urls(states)
  states.collect {|state| urlify(state)}
end
puts functional_urls(states).inspect


# Singles: Imperative Version
def imperative_singles(states)
  singles = []
  states.each do |state|
    if (state.split(/\s+/).length == 1)
      singles << state
    end
  end
  singles
end
puts imperative_singles(states).inspect


# Singles: Functional Version - Select
def functional_singles(states)
  states.select {|state| state.split.length == 1}
end
puts functional_singles(states).inspect


# Singles: Functional Version - Reject
def functional_singles_reject(states)
  states.reject {|state| state.split.length == 2}
end
puts functional_singles_reject(states).inspect


# Include: Functional Version
def include_doubles(states)
  states.select {|state| state.include? "Dakota"}
end
puts include_doubles(states).inspect


# Doubles: Functional Version
def functional_doubles(states)
  states.select {|state| state.split.length == 2}
end
puts functional_doubles(states).inspect


# Sum: Imperative Solution
def imperative_sum(numbers)
  total = 0
  numbers.each do |n|
    total += n
  end
  total
end
puts imperative_sum(numbers)


# Sum: Functional Solution
def functional_sum(numbers)
  numbers.inject {|total, n| total += n}
end
puts functional_sum(numbers)


# Lengths: Imperative Version
def imperative_lengths(states)
  lengths = {}
  states.each do |state|
    lengths[state] = state.length
  end
  lengths
end
puts imperative_lengths(states)


# Lengths: Functional Version
def functional_lengths(states)
  states.inject({}) do |lengths, state|
    lengths[state] = state.length
    lengths
  end
end
puts functional_lengths(states)


# Product: Functional Solution
def functional_product(numbers)
  numbers.inject {|total, n| total *= n}
end
puts functional_product(numbers)
