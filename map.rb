states = ["Missouri", "Kansas", "Wisconsin", "New Jersey"]

# Returns a URL-friendly version of a string
#  Example: "North Dakota" -> "north-dakota"
def urlify(string)
  string.downcase.split.join('-')
end


#URLs: Output
def urls_output(states)
states.map {|state| "https://example.com/#{urlify(state)}"}
end
puts urls_output(states).inspect
