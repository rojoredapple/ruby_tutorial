require 'date'

# Returns the day of the week for the given time object
def day_of_the_week(time)
  Date::DAYNAMES[time.wday]
end

def greeting(time)
  "Have a good day!"
end
